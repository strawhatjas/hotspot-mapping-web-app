import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AlertService } from '../../alert.service';

import { HotspotListComponent } from './hotspot-list.component';

describe('HotspotListComponent', () => {
  let component: HotspotListComponent;
  let fixture: ComponentFixture<HotspotListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HotspotListComponent ],
      imports: [
        HttpClientTestingModule, RouterTestingModule
      ],
      providers: [
        AlertService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HotspotListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have title', () => {
    let h1 = fixture.nativeElement.querySelector('h1');
    expect(h1.textContent).toEqual('Hotspot List');
  });

  it('should have table and correct headers for the list', () => {
    let table = fixture.nativeElement.querySelector('table');
    expect(table).not.toBeNull();

    let tableHeaders = fixture.nativeElement.querySelectorAll('th');
    expect(tableHeaders[0].textContent).toEqual('ID');
    expect(tableHeaders[1].textContent).toEqual('Location');
    expect(tableHeaders[2].textContent).toEqual('Site');
    expect(tableHeaders[3].textContent).toEqual('Start Date');
    expect(tableHeaders[4].textContent).toEqual('End Date');
    expect(tableHeaders[5].textContent).toEqual('Notes');
  });

  it('should have create new hotspot button', () => {
    let button = fixture.nativeElement.querySelector('#hotspot-list-create');
    expect(button.textContent).toEqual('Create New Hotspot');
  });

  it('should have refresh data button', () => {
    let button = fixture.nativeElement.querySelector('#hotspot-list-refresh');
    expect(button.textContent).toEqual('Refresh Data');
  });

  it('should have search field', () => {
    let field = fixture.nativeElement.querySelector('#hotspot-list-search');
    expect(field).not.toBeNull();
  });
});
