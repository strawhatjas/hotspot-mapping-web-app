import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  errorMessage: string = '';
  submitted: boolean = false;
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router) {

    this.form = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  ngOnInit(): void {
  }

  get formControl() {
    return this.form.controls;
  }

  get loginDetails(){
    return{
      "email": this.form.value.email,
      "password" : this.form.value.password
    }
  }

  login(): void {
    this.submitted = true;
    const val = this.form.value;
    this.errorMessage = '';
    if (val.email && val.password) {
      this.authService.login(val.email, val.password)
        .subscribe(
          res => {
            console.log("Success");
          },
          err => {
            console.error(err);
            this.errorMessage = "Incorrect username or password"
            this.submitted = false;
          },
          () => {
            this.submitted = false;
            console.log("User is logged in");
            this.router.navigateByUrl('/dashboard');
          }
        )
    }
  }
}